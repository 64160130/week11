package com.kittaphat.week11;

public interface Crawlable {
    public void crawl();
}
