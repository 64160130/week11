package com.kittaphat.week11;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {

        Bird bird = new Bird("Tweety");
        bird.eat();
        bird.sleep();
        bird.takeoff();
        bird.fly();
        bird.landing();

        Plane boeing = new Plane("Boeing", "Rosaroi");
        boeing.takeoff();
        boeing.fly();
        boeing.landing();

        Superman clark = new Superman("clark");
        clark.takeoff();
        clark.fly();
        clark.landing();
        clark.walk();
        clark.run();
        clark.eat();
        clark.sleep();
        clark.swim();

        Human man1 = new Human("man");
        man1.eat();
        man1.sleep();
        man1.run();
        man1.walk();
        man1.swim();

        Bat bat = new Bat("bat");
        bat.eat();
        bat.sleep();
        bat.takeoff();
        bat.fly();
        bat.landing();

        Rat rat = new Rat("rat");
        rat.eat();
        rat.sleep();
        rat.run();
        rat.walk();
        rat.swim();

        Cat cat = new Cat("cat");
        cat.eat();
        cat.sleep();
        cat.run();
        cat.walk();
        cat.swim();
        
        Dog dog = new Dog("dog");
        dog.eat();
        dog.sleep();
        dog.run();
        dog.walk();
        dog.swim();

        Snake snake = new Snake("snake");
        snake.eat();
        snake.sleep();
        snake.crawl();
        snake.swim();

        Fish fish = new Fish("fish");
        fish.eat();
        fish.sleep();
        fish.swim();

        Crocodile crocodile = new Crocodile("crocodile");
        crocodile.eat();
        crocodile.sleep();
        crocodile.crawl();
        crocodile.swim();

        Flyable[] flyables = {bird, boeing, clark, bat};
        for(int i=0; i<flyables.length;i++) {
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }
    
        Walkable[] walkables = {bird, clark, man1, rat, cat, dog};
        for(int i=0; i<walkables.length;i++) {
            walkables[i].walk();
            walkables[i].run();
        }

        Swimable[] swimables = {clark, man1, rat, cat, dog, fish, crocodile};
        for(int i=0; i<swimables.length;i++) {
            swimables[i].swim();
        }

        Crawlable[] crawlables = {snake,crocodile};
        for(int i=0; i<crawlables.length;i++) {
            crawlables[i].crawl();
        }
    }

}
