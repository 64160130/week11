package com.kittaphat.week11;

public interface Swimable {
    public void swim();
}
